<?php

/**
 * SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

namespace App\Twig;

use Twig\Error\Error;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppTransExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('l10n', [$this, 'l10n']),
            new TwigFilter('size', [$this, 'size']),
        ];
    }

    /**
     * @param array $array
     * @param string $locale
     * @return string
     * @throws Error
     */
    public function l10n(array $array, string $locale): string
    {
        if (!is_array($array)) {
            // No translations, nothing to do
            throw new Error("Error in l10n filter: not an array");
        }

        if (sizeof($array) === 0) {
            //print "Empty l10n array";
            return "";
        }

        if ($locale === 'en') {
            $locale = 'C';
        }

        if (array_key_exists($locale, $array)) {
            return $array[$locale];
        }
        return $array['C'];
    }

    /**
     * @param int $bytes
     * @param int $precision
     * @return string
     * @throws Error
     */
    public function size(int $bytes, int $precision = 2): string
    {
        $units = array('B', 'KB', 'MB', 'GB', 'TB'); 

        $bytes = max($bytes, 0); 
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024)); 
        $pow = min($pow, count($units) - 1); 

        $bytes /= pow(1024, $pow);

        return round($bytes, $precision) . ' ' . $units[$pow];
    }
}
